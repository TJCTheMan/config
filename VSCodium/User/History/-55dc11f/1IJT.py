# Copyright Tevin Coles 2023

"""
When you physically exercise to strengthen your heart, you
should maintain your heart rate within a range for at least 20
minutes. To find that range, subtract your age from 220. This
difference is your maximum heart rate per minute. Your heart
simply will not beat faster than this maximum (220 - age).
When exercising to strengthen your heart, you should keep your
heart rate between 65% and 85% of your heart’s maximum rate.
"""

#Function that takes the age input from the user, and calculates the minimum and maximum heart rate while exercising.
def heartrate(age):
    max_rate = 220 - age
    desired_min = (max_rate * 0.65)
    desired_max = (max_rate * 0.85)
    print(f"When you exercise to strengthen your heart, you should keep your heart rate between {desired_min:.0f} and {desired_max:.0f} beats per minute.")


age = int(input("Please enter your age: "))

heartrate(age)