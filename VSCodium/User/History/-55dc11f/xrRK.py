# Copyright Tevin Coles 2023

"""
When you physically exercise to strengthen your heart, you
should maintain your heart rate within a range for at least 20
minutes. To find that range, subtract your age from 220. This
difference is your maximum heart rate per minute. Your heart
simply will not beat faster than this maximum (220 - age).
When exercising to strengthen your heart, you should keep your
heart rate between 65% and 85% of your heart’s maximum rate.
"""

def heartrate(age):
    heartrate_max = 200 - age
    heartrate_desired_min = heartrate_max * 0.65
    return heartrate_desired_min
    heartrate_desired_max = heartrate_max * 0.85
    return heartrate_desired_max
    print(f"When you exercise to strengthen your heart, you should keep your heart rate between {heartrate_desired_min} and {heartrate_desired_max} beats per minute.")



age = int(input("Please enter your age: "))

heartrate(age)

