# Copyright Tevin Coles 2023 CSE111


"""
The time in seconds that a pendulum takes to swing back and
forth once is given by this formula:
             ____
            / h
    t = 2π / ----
          √  9.81

t is the time in seconds,
π is the constant PI, which is the ratio of the circumference
    of a circle divided by its diameter (use math.pi),
h is the length of the pendulum in meters.

Write a program that prompts a user to enter the length of a
pendulum in meters and then computes and prints the time in
seconds that it takes for that pendulum to swing back and forth.
"""
import math

def time_from_length(length):
    time_out = ((2*math.pi) * (math.sqrt(length / 9.81)))
    return time_out

print(f"This program takes the length of a pendulum, and calculates the time in seconds it takes to swing back and forth.\n")

pend_length = int(input(f"Input the length of the pendulum in meters: "))

time = time_from_length(pend_length)

print(f"Time: {time:.2f} seconds.")
