# Copyright Tevin Coles 2023 CSE111

import math

def boxes(items, items_per_box):

    boxes_needed = math.ceil(items/items_per_box)
    return boxes

items_input = int(input("Enter the number of items: "))
items_per_box_input = int(input("Enter the number of items per box: "))

boxes_needed = boxes(items_input,items_per_box_input)

print(f"For {items_input} items, packing {items_per_box_input} items in each box, you will need {boxes_needed} boxes.")