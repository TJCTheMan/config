# Copyright Tevin Coles 2023 CSE111

import math

def boxes(items, items_per_box):

    boxes_needed = math.ceil(items/items_per_box)
    global items_extra_box
    items_extra_box = items%items_per_box
    return boxes_needed


items_input = int(input("Enter the number of items: "))
items_per_box_input = int(input("Enter the number of items per box: "))

boxes_needed_output = boxes(items_input, items_per_box_input)

if items_extra_box == 0:
    leftover = "be full."
else:
    leftover = f"have {items_input - (items_per_box_input * (boxes_needed_output - 1))} item(s)."


print(f"For {items_input} items, packing {items_per_box_input} items in each box, you will need {boxes_needed_output} boxes. The last box will {leftover}")