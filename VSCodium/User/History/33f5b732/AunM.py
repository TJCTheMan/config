# Copyright Tevin Coles 2023 CSE111

"""

"""

def water_column_height(tower_height, tank_height):
    total_height = tower_height + ((3*tank_height) / 4)
    return total_height


def pressure_gain_from_water_height(height):
    pressure_gain = (998.2 * 9.806 * height) / 1000
    return pressure_gain

def pressure_loss_from_pipe(pipe_diameter, pipe_length, friction_factor, fluid_velocity):
