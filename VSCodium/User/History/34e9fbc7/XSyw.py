#Copyright Tevin Coles 2023 CSE111
import math

def calc_tire_volume(width, sidewall, rim):
    volume = (((math.pi)*(width ** 2)*(sidewall)*((width*sidewall)+(2540*rim)))/(10_000_000_000))
    return volume

print(f"This program takes the dimensions of your tire and calculates the volume of air inside of it. The size of a car tire in the United States is represented with three numbers like this: 205/60R15.\nThe first number (205)is the width of the tire in millimeters. \nThe second number (60) is the height of the sidewall of the tire. \nThe third number (15) is the diameter in inches of the wheel or rim that the tire fits. \n")

input_width = int(input("Enter the width of the tire in mm: "))
input_sidewall = int(input("Enter the sidewall height of the tire in mm: "))
input_rim = int(input("Enter the diameter of the rim in inches: "))

calc_tire_volume(input_width, input_sidewall, input_rim)

print(f"The approximate volume is {calc_tire_volume.volume} litres")