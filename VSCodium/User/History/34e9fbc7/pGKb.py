#Copyright Tevin Coles 2023 CSE111
import math

def calc_tire_volume(width, sidewall, rim):
    volume = (((math.pi)*(width ** 2)*(sidewall)*((width*sidewall)+(2540*rim)))/(10_000_000_000))
    return volume

print(f"This program takes the dimensions of your tire and calculates the volume of air inside of it. The size of a car tire in the United States is represented with three numbers like this: 205/60R15.\nThe first number (205)is the width of the tire in millimeters. \nThe second number (60) is the height of the sidewall of the tire. \nThe third number (15) is the diameter in inches of the wheel or rim that the tire fits. \n")

input_width = int(input("input the first number: "))
input_sidewall = int(input("input the first number: "))
input_rim = int(input("input the first number: "))