# Copyright Tevin Coles 2023 CSE111

def main():
    #Print the opening introductory statement.
    print(f"This program is an implementation of the Rosenberg\nSelf-Esteem Scale. This program will show you ten\nstatements that you could possibly apply to yourself.\nPlease rate how much you agree with each of the\nstatements by responding with one of these four letters:\n\nD means you strongly disagree with the statement.\nd means you disagree with the statement.\na means you agree with the statement.\nA means you strongly agree with the statement.\n\n")

    global questions_list
    questions_list = ["I feel that I am a person of worth, at least on an equal plane with others.","I feel that I have a number of good qualities.", "All in all, I am inclined to feel that I am a failure.", "I am able to do things as well as most other people.", "I feel I do not have much to be proud of.", "I take a positive attitude toward myself.", "On the whole, I am satisfied with myself.", "I wish I could have more respect for myself.", "I certainly feel useless at times.", "At times I think I am no good at all."]

    scores = []
    for i in range(0, len(questions_list)):
        user_answer = q_and_a(i)
        scores.append(user_answer)

    print(scores)
        

def q_and_a(q_num):
    print(f"{q_num+1}. {questions_list[q_num]}")
    user_input = input("Enter D, d, a, or A: ")
    return user_input

    


if __name__ == "__main__":
    main()