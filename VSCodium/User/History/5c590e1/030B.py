# Copyright Tevin Coles 2023 CSE111

def main():
    #Print the opening introductory statement.
    print(f"This program is an implementation of the Rosenberg\nSelf-Esteem Scale. This program will show you ten\nstatements that you could possibly apply to yourself.\nPlease rate how much you agree with each of the\nstatements by responding with one of these four letters:\n\nD means you strongly disagree with the statement.\nd means you disagree with the statement.\na means you agree with the statement.\nA means you strongly agree with the statement.")


if __name__ == "__main__":
    main()