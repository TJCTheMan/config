# Copyright Tevin Coles 2023 CSE111

from datetime import datetime
# Import datetime so that it can be
# used to compute a person's age.
from datetime import datetime


def main():
    birthdate_input = input("input your birthdate in the following format: YYYY-MM-DD: ")
    gender_input = input("Input your gender (male, female): ")
    height_input = input("Input your height in inches: ")
    weight_input = input("Input your weight in pounds: ")

    age_out = compute_age(birthdate_input)
    weight_kg = kg_from_lb(weight_input)
    height_cm = cm_from_in(height_input)

    bmi_out = body_mass_index(weight_kg, height_cm)
    bmr_out = basal_metabolic_rate(gender_input.lower(), weight_kg, height_cm, age_out)

    print(f"Your age is {age_out} years old.")
    print(f"Your weight is {weight_kg:.2f} kg.")
    print(f"Your height is {height_cm:.1f} cm.")
    print(f"Your BMI is {bmi_out:.2f}.")
    print(f"Your BMR is {bmr_out:.2f}.")
    
    pass


def compute_age(birth_str):
    """Compute and return a person's age in years.
    Parameter birth_str: a person's birthdate stored
        as a string in this format: YYYY-MM-DD
    Return: a person's age in years.
    """
    # Convert a person's birthdate from a string
    # to a date object.
    birthdate = datetime.strptime(birth_str, "%Y-%m-%d")
    today = datetime.now()

    # Compute the difference between today and the
    # person's birthdate in years.
    years = today.year - birthdate.year

    # If necessary, subtract one from the difference.
    if birthdate.month > today.month or \
        (birthdate.month == today.month and \
            birthdate.day > today.day):
        years -= 1

    return years


def kg_from_lb(pounds):
    """Convert a mass in pounds to kilograms.
    Parameter pounds: a mass in U.S. pounds.
    Return: the mass in kilograms.
    """
    kg = pounds *  0.45359237 

    return kg


def cm_from_in(inches):
    """Convert a length in inches to centimeters.
    Parameter inches: a length in inches.
    Return: the length in centimeters.
    """
    return


def body_mass_index(weight, height):
    """Compute and return a person's body mass index.
    Parameters
        weight: a person's weight in kilograms.
        height: a person's height in centimeters.
    Return: a person's body mass index.
    """
    return


def basal_metabolic_rate(gender, weight, height, age):
    """Compute and return a person's basal metabolic rate.
    Parameters
        weight: a person's weight in kilograms.
        height: a person's height in centimeters.
        age: a person's age in years.
    Return: a person's basal metabolic rate in kcals per day.
    """
    if gender.lower() == 'female':
        bmr = 447.593 + (9.247 * weight) + (3.098 * height) - (4.330 * age)
    elif gender.lower() == 'male':
        bmr = 88.362 + (13.397 * weight) + (4.799 * height) - (5.677 * age)

    


    return


# Call the main function so that
# this program will start executing.


"""

"""

def bmi_calc(height,weight):
    bmi = (10_000 * weight)/(height ** 2)
    return bmi

def bmr_calc(gender, birthdate, height, weight):

    age = datetime.now() - birthdate
    if gender.lower() == 'female':
        bmr = 447.593 + (9.247 * weight) + (3.098 * height) - (4.330 * age)
    elif gender.lower() == 'male':
        bmr = 88.362 + (13.397 * weight) + (4.799 * height) - (5.677 * age)

    



