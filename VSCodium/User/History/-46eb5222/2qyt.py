# Copyright Tevin Coles 2023 CSE111

"""

"""

def bmi_calc(height,weight):
    bmi = (10_000 * weight)/(height ** 2)
    return bmi

def bmr_calc(gender, age, height, weight):

    if gender.lower() == 'female':
        bmr = 447.593 + (9.247 * weight) + (3.098 * height) - (4.330 * age)
    elif gender.lower() == 'male':
        bmr = 88.362 + (13.397 * weight) + (4.799 * height) - (5.677 * age)



