def main():
    # Get an odometer value in U.S. miles from the user.
    odo_start = int(round(input("Enter the first odometer reading (miles): ")))

    # Get another odometer value in U.S. miles from the user.
    odo_end = int(round(input("Enter the second odometer reading (miles): ")))

    # Get a fuel amount in U.S. gallons from the user.
    fuel_gal = int(round(input("Enter the amount of fuel used (gallons): ")))

    # Call the miles_per_gallon function and store
    # the result in a variable named mpg.

    mpg = miles_per_gallon(odo_start, odo_end, fuel_gal)

    # Call the lp100k_from_mpg function to convert the
    # miles per gallon to liters per 100 kilometers and
    # store the result in a variable named lp100k.

    lp100k = lp100k_from_mpg(mpg)

    # Display the results for the user to see.

    print(mpg, lp100k)

    pass


def miles_per_gallon(start_miles, end_miles, amount_gallons):
    """Compute and return the average number of miles
    that a vehicle traveled per gallon of fuel.

    Parameters
        start_miles: An odometer value in miles.
        end_miles: Another odometer value in miles.
        amount_gallons: A fuel amount in U.S. gallons.
    Return: Fuel efficiency in miles per gallon.
    """
    mpg_out = round(((odo_end - odo_start)/fuel_gal), 2)


    return mpg_out


def lp100k_from_mpg(mpg):
    """Convert miles per gallon to liters per 100
    kilometers and return the converted value.

    Parameter mpg: A value in miles per gallon
    Return: The converted value in liters per 100km.
    """
    return


# Call the main function so that
# this program will start executing.
main()