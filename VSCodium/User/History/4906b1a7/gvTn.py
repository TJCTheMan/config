# Copyright Tevin Coles 2023 CSE111

from datetime import datetime


def tax(subtotal):
    tax_out = subtotal * 0.06
    return tax_out


def total(subtotal, tax):
    total_out = subtotal + tax
    return total_out

subtotal = 0
subtotal_item = None
while subtotal_item != 0:
    subtotal_item = float(input(f"Please enter the cost of the item: "))
    quantity_item = float(input(f"Please enter how many of this item you have: "))
    subtotal += (subtotal_item * quantity_item)
    print(f"To end, input 0 for the price and the quantity.")


date_now = datetime.now()

if (date_now.weekday() == 1 or date_now.weekday() == 2):
    discount_day = True
    
#FOR TESTING PURPOSES 
#discount_day = False

if (discount_day == True and subtotal >= 50):
    discount = subtotal * 0.1
    subtotal = subtotal - discount


tax_amount = tax(subtotal)
total_amount = total(subtotal, tax_amount)


if discount_day == True:
    print(f"Discount amount: ${discount:.2f}")
print(f"Tax amount: ${tax_amount:.2f}")
print(f"Total amount: ${total_amount:.2f}")

