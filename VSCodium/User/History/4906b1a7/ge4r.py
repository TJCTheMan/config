# Copyright Tevin Coles 2023 CSE111

"""

"""
from datetime import datetime


def tax(subtotal):
    tax_out = subtotal * 0.06
    return tax_out


def total(subtotal, tax):
    total_out = subtotal + tax
    return total_out

subtotal_input = float(input(f"Please enter the subtotal: "))

date_now = datetime.now()

if (date_now.weekday() == 1 or date_now.weekday() == 2):
    discount_day = True
    
if (discount_day == True and subtotal_input >= 50):
    discount = subtotal_input * 0.1
    subtotal_input = subtotal_input - discount

discount_day = False

tax_amount = tax(subtotal_input)
total_amount = total(subtotal_input, tax_amount)


if discount_day == True:
    print(f"Discount amount: ${discount:.2f}")
print(f"Tax amount: ${tax_amount:.2f}")
print(f"Total amount: ${total_amount:.2f}")

