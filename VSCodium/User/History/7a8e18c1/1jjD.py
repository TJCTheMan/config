# Copyright Tevin Coles 2023 CSE111

"""
This program reads the contents
of students.csv, stores numbers, and checks input against them.

"""

def main():

def read_dictionary(filename):
    """Read the contents of a CSV file into a
    dictionary and return the dictionary.

    Parameters
        filename: the name of the CSV file to read.
    Return: a dictionary that contains
        the contents of the CSV file.
    """