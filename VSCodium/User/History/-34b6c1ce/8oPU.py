# Copyright Tevin Coles 2023 CSE111

"""
This program reads the contents
of provinces.txt into a list and then modifies the list and outputs the results.

"""

def main():

    file_as_list=read_list("/home/tj/school/programming-with-functions/w9/provinces.txt")


    file_as_list.pop(0)
    file_as_list.pop()
    AB_Count = 0
    for item in file_as_list:
        
        if item == "AB":
            i = file_as_list.index(item)
            file_as_list[i] = "Alberta"

    for item in file_as_list:
        if item == 'Alberta':
            AB_Count += 1
    
    print(file_as_list)
    print(f"There are {AB_Count} instances of Alberta")



def read_list(filename):
    """Read the contents of a text file into a list and
    return the list. Each element in the list will contain
    one line of text from the text file.

    Parameter filename: the name of the text file to read
    Return: a list of strings
    """
    # Create an empty list that will store
    # the lines of text from the text file.
    text_list = []

    # Open the text file for reading and store a reference
    # to the opened file in a variable named text_file.
    with open(filename, "rt") as text_file:

        # Read the contents of the text
        # file one line at a time.
        for line in text_file:

            # Remove white space, if there is any,
            # from the beginning and end of the line.
            clean_line = line.strip()

            # Append the clean line of text
            # onto the end of the list.
            text_list.append(clean_line)

    # Return the list that contains the lines of text.
    return text_list

if __name__ == "__main__":
    main()


