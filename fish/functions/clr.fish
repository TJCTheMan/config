function clr --wraps='clear & bash' --description 'alias clr=clear & bash'
  clear & bash $argv; 
end
