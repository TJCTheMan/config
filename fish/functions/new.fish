function new --wraps='clear & cd & bash' --description 'alias new=clear & cd & bash'
  clear & cd & bash $argv; 
end
