# This is TJ's Config files

This repository holds/clones my basic .config folder as it stands on most of my linux installs. These configs are set up for Arch and Arch-Based systems.

# ***WIP*** 

*packages.txt holds the list of my currently installed packages, I hope.* 

This is clearly a work in progress, but should be clonable into any Arch install home directory, and as long as the packages in packages.txt are installed, everything should work correctly.
